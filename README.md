# README #

## Solving problems in DIMACS CNF format ##

```
python3 Dimacs.py <input file> <output file>
```

The result will be written both on the standard output
(human readable form) and in the output file.
