#!/usr/bin/env python3

from Propagation import *
from random import *

def DPLL_CNF (cnf, valuation = []):
    # NB: cnf and valuation are passed by REFERENCE!
    valuation = valuation.copy()
    # * if Φ is a consistent set of literals then we can short-circuit here,
    #   but this is unnecessary as literal propagation below will do it for us.
    # * if Φ contains an empty clause then we can return here,
    #   but this is again unnecessary for the same reason.

    # 1.a remove all units
    i = 0
    while i < len(cnf):
        clause = cnf[i]
        if 1 == len(clause):
            # We have a unit!
            literal = clause[0]
            valuation.append (literal)
            cnf = propagateLiteral (cnf, literal)
            if False == cnf: # We had an empty clause
                return [False, valuation]
            # Reset loop
            i = 0
        else:
            i += 1
    # 1.b find pure literals (they only appear in one state (+ or -) everywhere
    # Skipped due to being non-trivial to implement

    # If cnf is empty, we are done!
    if [] == cnf:
        return [True, valuation]
    # empty clause in CNF was already tested above

    # if we are still here, pick one variable and recursively test both states
    clause_choice = choice (cnf)
    literal = choice (clause)

    left_cnf = propagateLiteral (cnf, literal)
    valuation.append (literal)
    left_res, left_val = DPLL_CNF (left_cnf, valuation)
    if True == left_res:
        return [True, left_val]

    right_cnf = propagateLiteral (cnf, -literal)
    valuation.pop()
    valuation.append (-literal)
    return DPLL_CNF (left_cnf, valuation)
    # Sadly no other option.


if __name__ == "__main__":
    # [unsatisfiable]  (a ∨ b) ∧ (⌐a ∨ c) ∧ (⌐c ∨ d)
    clauses = [[1,2], [-1, 3], [-3, 4]]
    s,v = DPLL_CNF (clauses)
    print(s,v)

    # [unsatisfiable]  ⌐a ∧ ⌐b
    clauses = [[-1], [-2]]
    s,v = DPLL_CNF (clauses)
    print(s,v)
