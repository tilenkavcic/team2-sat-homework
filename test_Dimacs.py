#!/usr/bin/env python3
import unittest
# https://stackoverflow.com/questions/11536764/how-to-fix-attempted-relative-import-in-non-package-even-with-init-py/27876800#27876800
import sys
import os
#sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )

# The following two function bodies have largely been copied from Janos Vidali's repo.
def readSolution(f):
    try:
        sol = [int(x) for x in f.readline().split()]
        print ("Solution in file: " + str(sol))
        if sol == [0]:
            print("No satisfying valuation found")
            #sys.exit(1)
            return [0]
        if not all(-x not in sol for x in sol):
            return 3
    except (ValueError, AssertionError):
        print("Malformed solution")
        #sys.exit(2)
        return 2
    return sol

def compareCorrect(f, sol):
    for l in f:
        if len(l) == 0 or l[0] == 'c':
            continue
        if l[0] == 'p':
            _, _, n, _ = l.split()
            try:
                n = int(n)
            except ValueError:
                print("Malformed formula")
                #sys.exit(3)
                return 3
            if len(sol) > 0 and (min(sol) < -n or max(sol) > n):
                print("The solution has too many atoms")
                #sys.exit(2)
                return 2
        else:
            try:
                if not any(int(x) in sol for x in l.split()):
                    print("The solution is incorrect")
                    #sys.exit(1)
                    return 1
                # verify that our solution has no duplicates
                sol_dedupe = list(set(sol))
                if not (len(sol) == len(sol_dedupe)):
                    print("The solution has duplicate atoms")
                    return 1

                # verify that our solution has no +lit -lit appearing at the same time
                if (not [0] == sol) and (not all(-x not in sol for x in sol)):
                    return 4
            except ValueError:
                print("Malformed formula")
                #sys.exit(3)
                return 3
    print("The solution is correct")
    return 0

def run_solver (test_f):
    #retval = os.system("./Dimacs.py " + test_f + " result")
    retval = os.system("./cppsat " + test_f + " result")

    result_f = open("result", 'r')
    sol = readSolution (result_f)
    result_f.close()
    os.remove("result")
    return sol

def compare_results (sol, correct):
    print ("Testing against solution " + str(correct))

    correct_f = open(correct, 'r')
    retval = compareCorrect (correct_f, sol)
    correct_f.close()

    print ("Retval: " + str(retval))
    return retval



class TestDimacs (unittest.TestCase):

    def __init__(self, input_file):
        super(TestDimacs, self).__init__()
        self.input_file = input_file

    def runTest (self):
        print ("Testing " + str(self.input_file))
        sol = run_solver (self.input_file)
        # remove .txt and append ...
        solution_f = self.input_file[:-4] + "_solution.txt"
        if os.path.exists (solution_f):
            res = compare_results (sol, solution_f)
            self.assertTrue (0 == res)
            return

        sol_id = 1
        while True:
            solution_f = self.input_file[:-4] + "_solution" + str(sol_id) + ".txt"
            print ("Trying solution file " + str(solution_f))
            if os.path.exists (solution_f):
                res = compare_results (sol, solution_f)
                if 0 == res:
                    self.assertTrue (True)
                    break
            else:
                self.assertTrue (False) # no solution matches
                break
            sol_id += 1


def proc_dir (suite, d):
    for root, dirs, files in os.walk(d):
        print ("root: ", str(root))
        print ("dirs: ", str(dirs))
        print ("files: ", str(files))

        for in_f in files:
            if "solution" in in_f or "README" in in_f:
                continue
            in_f = root + os.path.sep + in_f

            suite.addTest (TestDimacs (in_f))

        for d in dirs:
            proc_dir (suite, root + os.path.sep + d)


if __name__ == '__main__':
    suite = unittest.TestSuite()
    proc_dir (suite, 'tests')
    unittest.TextTestRunner().run(suite)
