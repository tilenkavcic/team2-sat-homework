#!/usr/bin/env python3

def propagateLiteral (cnf, literal):
    """
    Applies literal (integer) in the given state(+/-) to all clauses and
    returns simplified CNF or [] in case of failure
    """
    new_cnf = []
    for clause in cnf:
        new_clause = []

        for clit in clause:
            if clit == literal:
                # If literal exists in the same state => clause is true and not needed
                break
            elif clit == -literal:
                # other state => remove it from clause
                continue
            else:
                new_clause.append(clit)
        else: # only executed when the above finishes normally (i.e. no break)
            if 0 == len(new_clause): # oops, CNF = Falsom!
                return False

            new_cnf.append (new_clause)

    return new_cnf


if __name__ == "__main__":
    # (a ∨ b) ∧ (⌐a ∨ c) ∧ (⌐c ∨ d) ∧ a
    cnf = [ [1, 2], [-1, 3], [-3, 4], [1] ]
    print(cnf)
    cnf = propagateLiteral (cnf, 1)
    print(cnf)
    cnf = propagateLiteral (cnf, -3)
    print(cnf)
    print("Test pass? " + str (False == cnf))

    print("")
    # a ∧ (⌐a ∨ ⌐b)
    cnf = [ [1], [-1, -2] ]
    print(cnf)
    cnf = propagateLiteral (cnf, 1)
    print(cnf)
    cnf = propagateLiteral (cnf, -2)
    print(cnf)
    print("Test pass? " + str ([] == cnf))
