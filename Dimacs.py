#!/usr/bin/env python3

import sys

def error (msg):
    print ("Error: ", msg)
    sys.exit()


def readfile_cnf (path):
    cnf = []
    f = open(path, 'r')

    for line in f:
        if 0 == len (line):
            continue

        if 'c' == line[0]: # comment
            continue

        tokens = line.rstrip().split(' ')
        if "p" == tokens[0]: # problem definition TODO checking
            continue
        elif 0 == int(tokens[-1]): # clause
            # note: int(tokens[-1] as tokens contains something else than 0,
            # perhaps a newline?
            clause = []
            for i in range(0, len(tokens)-1):
                literal = int(tokens[i])
                clause.append (literal)

            cnf.append(clause)
        else:
            error ("Invalid line in DIMACS input: " + str(line))
    f.close()
    return cnf


def writefile_cnf (path, valuation):
    f = open(path, 'a')

    for entry in valuation:
        f.write (str(entry))
        f.write (" ")

    f.write ('\n')
    f.close()



if __name__ == "__main__":
    from Solver import *
    import os.path

    if 3 != len (sys.argv):
        error ("Usage: python3 Dimacs.py <input_file> <output_file>")
    if not os.path.isfile (sys.argv[1]):
        error ("Input file does not exist!")
    if os.path.isfile (sys.argv[2]):
        error ("Output file already exists!")

    cnf = readfile_cnf (sys.argv[1])
    #print ("Solving: " + str(cnf))
    is_satisfiable, valuation = DPLL_CNF(cnf)

    print ("Satisfiable? " + str(is_satisfiable))
    print ("Valuation: " + str(valuation))

    if is_satisfiable: # write valuation to file
        writefile_cnf (sys.argv[2], valuation)
    else:
        writefile_cnf (sys.argv[2], [0])
        #writefile_cnf (sys.argv[2], And(0))
