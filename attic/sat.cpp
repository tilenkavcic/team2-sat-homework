#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdarg.h>
#include <stdexcept>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>
using std::make_pair;
using std::string;
using std::cout;
using std::endl;
using std::ostream;


// Base case
static void debug (int severity, std::ostringstream& os)
{
	if (0 == severity)
		throw std::runtime_error (os.str());
	else if (1 > severity) // discard debug messages
		std::cerr << os.str() << endl;
}
// Variable arguments recursion case
template<typename T, typename... Arguments>
static void debug (int severity, std::ostringstream& os,
                   T first, Arguments ... params)
{
	os << first;
	debug (severity, os, params...);
}
// Variable arguments case (entry point)
template<typename T, typename... Arguments>
static void debug (int severity, T first, Arguments ... params)
{
	std::ostringstream os;
	os << first;
	debug (severity, os, params...);
}



// CNF is a conjuntion of disjunctions
// in this implementation, the CNF class contain clauses,
// which are sets of integers (negative means negated)
typedef std::unordered_set<int> Clause;

ostream& operator<< (ostream& out, Clause& clause)
{
	unsigned s = clause.size();
	for (int i : clause) {
		if (0 == --s)
			out << i;
		else
			out << i << " | ";
	}
	return out;
}



// New graph-based structure, old one is below
// Since a single literal can be contained in multiple clauses
// and a single clause can have multiple literals, it is not possible
// to contain one in the other, thus ... pointer fun!
struct CNF2
{
	// Forward declarations for class cyclic recursion
	struct Literal;
	struct Clause;

	// Comparator for storage inside the graph
	struct InnerClauseCompare {
		bool operator() (const Clause* lhs, const Clause* rhs) const {
			return lhs->id < rhs->id;
		}
	};
	struct InnerLiteralCompare {
		bool operator() (const Literal* lhs, const Literal* rhs) const {
			return lhs->id < rhs->id;
		}
	};


	struct Literal {
		typedef std::set<Clause*, InnerClauseCompare> ClauseRefs;
		const int id;
		ClauseRefs positive; // Clauses where it appears non-negated
		ClauseRefs negative; // Clauses where it appears negated

		Literal(int _id):id(_id) {}
		int size() const { return positive.size() + negative.size(); }
		int min_size() const { return std::min (positive.size(), negative.size()); }
	};

	struct Clause {
		const int id;
		typedef std::set<Literal*, InnerLiteralCompare> LiteralRefs;
		LiteralRefs positive;
		LiteralRefs negative; // literals that are negated

		Clause(int _id):id(_id) {}
		int size() const { return positive.size() + negative.size(); }
	};

	// Clauses should be sorted such that it is easy to find units
	// e.g. first sort by .size(), then by id.
	// and such that it is easy to find pure variables, but that is handled
	// by the literals above
	struct OuterClauseCompare {
		bool operator() (const Clause* lhs, const Clause* rhs) const {
			int size_diff = lhs->size() - rhs->size();
			if (0 == size_diff)
				return lhs->id < rhs->id;
			else
				return size_diff;
		}
	};
	// We want to have pure vars on quick access
	// They will be near front
	// Unfortunately it is damn hard to construct the graph if you
	// cannot easily find already-inserted literals.
	struct OuterLiteralCompare {
		bool operator() (const Literal* lhs, const Literal* rhs) const {
//			int min_size_diff = lhs->min_size() - rhs->min_size();
//			if (0 == min_size_diff)
				return lhs->id < rhs->id;
//			else
//				return min_size_diff;
		}
	};

	typedef std::set<Clause*, OuterClauseCompare> ClauseRefs;
	ClauseRefs clauses;
	typedef std::map<int, Literal*> LiteralRefs;
	LiteralRefs literals;
	//typedef std::map<int, Literal*> LiteralIntRefs;
	//LiteralIntRefs literal_int_refs;
	unsigned clause_count = 0;

	void addClause (std::vector<int>& literal_vec) {
		Clause* clause = new Clause(clause_count++);
		// TODO .reserve()
		for (int lit : literal_vec) {
			int abs_lit = std::abs (lit);
			// Try to find it
			LiteralRefs::iterator l_it = literals.find (lit);
			Literal* literal;
			if (literals.end() == l_it) {
				literal = new Literal(abs_lit);
				literals.insert (make_pair (abs_lit, literal));
			} else {
				literal = l_it->second;
			}

			// update pointers
			if (0 < lit) {
				clause->positive.insert (literal);
				literal->positive.insert (clause);
			} else {
				clause->negative.insert (literal);
				literal->negative.insert (clause);
			}
		}

		clauses.insert (clause);
	}

	bool propagateLiteral (int lit)
	{
		debug (3, "Propagating literal ", lit);
		// Find it
		int abs_lit = std::abs (lit);
		LiteralRefs::iterator l_it = literals.find (abs_lit);
		if (literals.end() == l_it)
			debug (0, "Literal not found!");

		Literal* literal = l_it->second;
		literals.erase (l_it);

		Literal::ClauseRefs *same_clauses, *neg_clauses;
		if (0 < lit) {
			same_clauses = &(literal->positive);
			neg_clauses = &(literal->negative);
		} else {
			same_clauses = &(literal->negative);
			neg_clauses = &(literal->positive);
		}

		// Remove all clauses that contain it in the same form
		for (Clause* clause : *same_clauses) {
			clause->positive.erase (literal);
			clause->negative.erase (literal);
			// Update other literal's clause lists
			for (Literal* other_lit : clause->positive)
				other_lit->positive.erase (clause);
			for (Literal* other_lit : clause->negative)
				other_lit->negative.erase (clause);
			clauses.erase (clause);
		}
		// Remove all literals in the other form
		for (Clause* clause : *neg_clauses) {
			clause->positive.erase (literal);
			clause->negative.erase (literal);
			if (0 == clause->size())
				return false;
		}

		return true;
	}
};

struct CNF
{
	struct UnitClauseHints
	{
		typedef std::vector<unsigned> UnitClausesStorage;
		UnitClausesStorage unit_clauses;

		void addHint (unsigned clause_id) {
			unit_clauses.push_back (clause_id);
		}
		bool hasNextHint ()
		{ return !unit_clauses.empty(); }
		unsigned nextHint () {
			unsigned clause_id = unit_clauses.back();
			unit_clauses.pop_back();
			return clause_id;
		}
	};


	typedef std::unordered_map<unsigned, Clause> ClauseStorage;

	void addClause (Clause& clause) {
		unsigned id = clauses.size(); // Allocate next available ID
		clauses.insert(make_pair(id, clause));
		// If necessary, add it to unit hints
		if (1 == clause.size())
			unitHints.addHint (id);
	}
	// modify cnf structure assuming the literal is in the given state
	// Returns whether the CNF is still OK (i.e.no empty clauses)
	bool propagateLiteral (int literal)
	{
		debug (3, "Propagating literal ", literal);
		for (ClauseStorage::iterator it = clauses.begin(); it != clauses.end(); ) {
			Clause& clause = it->second;
			debug (4, "Looping over clause ", clause);

			if (0 != clause.count (literal)) { // Whole clause is T
				it = clauses.erase(it); // returns ++it
			} else if (0 != clause.erase (-literal)) { // remove just this part
				unsigned size = clause.size();
				if (0 == size)
					return false;
				else if (1 == size)
					unitHints.addHint (it->first);
				++it;
			} else { // This clause is OK
				++it;
			}
		}
		return true;
	}

	ClauseStorage clauses;
	UnitClauseHints unitHints;
};

ostream& operator<< (ostream& out, CNF& cnf)
{
	unsigned s = 0;
	for (auto it : cnf.clauses) {
		if (0 == s++)
			out << "Clauses: (" << it.second << ")" << endl;
		else
			out << "       & (" << it.second << ")" << endl;
	}
	return out;
}


// Stores valuations, i.e. assignmnets of variables
// every run of the DPLL algoritm shall add at least one valuation
// to the end of the vector
typedef std::vector<int> Valuations;

bool DPLL (Valuations& valuations, CNF& cnf)
{
	debug (1, "Starting DPLL iteration, CNF: ", cnf);
	// Get rid of all unit clauses
	while (cnf.unitHints.hasNextHint ()) {
		unsigned clause_id = cnf.unitHints.nextHint();
		// Find this clause
		auto it = cnf.clauses.find (clause_id);
		// The same literal might have been added more than once:
		// e.g. (-3 | 4) & (-5 | 4) after propagating 3 and 5.
		if (cnf.clauses.end() == it)
			continue;
		// Extract the only literal out of it
		int literal = *((it->second).begin());
		debug (2, "Processing unit-literal ", literal);
		// Propagate this unit
		if (! cnf.propagateLiteral (literal))
			return false;
		valuations.push_back(literal);
	}

	// TODO: pure variables

	// Is the CNF empty?
	if (cnf.clauses.empty())
		return true;

	// Pick a literal and recurse on it
	int literal = *(cnf.clauses.begin()->second.begin());
	unsigned valuations_size = valuations.size();

	// First try using literal in the state in which it appears
	// Force copying CNF for now
	valuations.push_back(literal);
	CNF cnf_lsame (cnf);
	cnf_lsame.propagateLiteral (literal);
	if (DPLL (valuations, cnf_lsame))
		return true;
	// Failed, use the other state
	CNF cnf_ldiff (cnf);
	cnf_ldiff.propagateLiteral (-literal);
	while (valuations.size() > valuations_size)
		valuations.pop_back();
	valuations.push_back(-literal);
	return DPLL (valuations, cnf_ldiff);
}



void read_cnf (const char* path, CNF& cnf)
{
	std::ifstream dimacs_file (path);
	if (! dimacs_file.is_open() )
		debug (0, "Unable to open input DIMACS file at", path);

	string line;
	int nbvars = -1;
	int nbclauses = -1;
	for (unsigned line_counter = 1; std::getline (dimacs_file, line); ++line_counter) {
		if (0 == line.size()) // empty line
			continue;
		if ('c' == line.at(0)) // Comment
			continue;
		else if ('p' == line.at(0)) { // Header
			if (-1 != nbvars || -1 != nbclauses)
				debug (0, path, ":", line_counter, ": multiple problems in input!");
			if (2 != sscanf (line.c_str(), "p cnf %d %d", &nbvars, &nbclauses))
				debug (0, path, ":", line_counter,
				  ": Problem not in CNF format or nbvars/nbclauses not provided");

			debug (1, "nvars: ", nbvars, ", nbclauses: ", nbclauses);
		} else { // Clause definition or invalid line
			std::istringstream in (line);
			Clause clause;
			for (int literal; in >> literal; ) {
				if (0 == literal)
					break;
				clause.emplace (literal);
			}

			if (in.bad())
				debug (0, path, ":", line_counter, ": Error occured while parsing clause");
			else if (in.eof())
				; // all good!
			else if (in.fail())
				debug (0, path, ":", line_counter, ": Clause countains non-numbers!");
			cnf.addClause (clause);
		}
	}
}

int main (int argc, char **argv)
{
	std::srand (std::time(0));
	// Read dimacs file.
	if (3 != argc)
		debug (0, "usage: ", argv[0], " <path_to_dimacs_file> <path_to_output_file>");

	CNF cnf;
	read_cnf (argv[1], cnf);

	Valuations valuations;
	bool success = DPLL (valuations, cnf);

	const char* opath = argv[2];
	std::ofstream out_file (opath);
	if (! out_file.is_open() )
		debug (0, "Unable to open ", opath, " for writing.");
	if (success) {
		// Write to file
		for (int i : valuations)
			out_file << i << ' ';
		out_file << '\n';
	} else {
		out_file << '0';
	}

	return 0;
}
