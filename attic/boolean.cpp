#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <map>
#include <stdexcept>
#include <initializer_list>


using std::vector;
using std::string;
using std::runtime_error;

struct Formula;
struct Variable;
typedef std::map<string, bool> Variables;

struct Formula
{
   virtual ~Formula()
   { throw runtime_error ("Formula::~Formula is not to be called"); }

   virtual bool eval (Variables&) const = 0;

   // Return a simplified expression
   // the tree will be fully copied so that deleting the
   // original one will not affect it
   virtual Formula* simplify () const = 0;

   // Return a copy of the entire contained tree
   virtual Formula* copy() const = 0;
};

typedef std::vector<Formula*> ManyFormulaPtrs;
typedef std::initializer_list<Formula*> FormulaInitList;
// and{p} = p
// or{p} = p
// and{} = T
// or{} = falsom
struct Falsom : public Formula
{
   virtual bool eval (Variables&) const override
   { return false; }

   virtual Formula* simplify () const override
   { return copy(); }

   virtual Formula* copy() const override
   { return new Falsom; }
};


struct Top : public Formula
{
   virtual bool eval (Variables&) const override
   { return true; }

   virtual Formula* simplify () const override
   { return copy(); }

   virtual Formula* copy() const override
   { return new Top; }
};


struct And : public Formula
{
   And() {}
   And(FormulaInitList l): sub(l) {}
   And(ManyFormulaPtrs l): sub(l) {}
   // Shalow copy constructor
//   And(And &a) {

   virtual bool eval (Variables& vars) const override
   {
      for (Formula* formula : sub) {
         if (false == formula->eval (vars))
            return false;
      }
      return true;
   }

   virtual Formula* simplify () const override
   {
      And* newAnd = new And();

      for (Formula* formula : sub) {
	 // poenostavi vse spodaj
	 Formula* newf = formula->simplify();
         // if Falsom in sub, return F
         if (dynamic_cast<Falsom*> (newf))
            return new Falsom();
	 newAnd->sub.push_back (newf);
      }

      return newAnd;
   }

   virtual Formula* copy() const override
   {
      And* newAnd = new And();
      for (Formula* formula : sub)
	 newAnd->sub.push_back (formula->copy());
      return newAnd;
   }

   ManyFormulaPtrs sub;
};


struct Or : public Formula
{
   Or () {}
   Or (FormulaInitList l): sub(l) {}
   Or (ManyFormulaPtrs l): sub(l) {}

   virtual bool eval (Variables& vars) const override
   {
      for (Formula* formula : sub) {
         if (true == formula->eval (vars))
            return true;
      }
      return false;
   }

   virtual Formula* simplify () const override
   {
      Or* newOr = new Or();

      for (Formula* formula : sub) {
	 // poenostavi vse spodaj
	 Formula* newf = formula->simplify();
         // if Top in sub, return Top
	 if (dynamic_cast<Top *>(newf))
            return new Top();
	 newOr->sub.push_back (newf);
      }
      return newOr;
   }

   virtual Formula* copy() const override
   {
      Or* newOr = new Or();
      for (Formula* formula : sub)
	 newOr->sub.push_back (formula->copy());
      return newOr;
   }

   // eq(self,other):
   //   return isinstance (
   ManyFormulaPtrs sub;
};


struct Implication : public Formula
{
   Implication(Formula* _lhs, Formula* _rhs): lhs(_lhs), rhs(_rhs) {}

   virtual bool eval (Variables& vars) const override
   {
      bool rlhs = lhs->eval (vars);
      bool rrhs = rhs->eval (vars);
      if (rlhs && !rrhs)
	 return false;
      else
	 return true;
   }

   virtual Formula* simplify () const override
   {
      // poenostavi vse spodaj
      Formula* newLhs = lhs->simplify();
      Formula* newRhs = rhs->simplify();
      // 1->0  results in F, else T
      if (dynamic_cast<Falsom *>(newLhs)
	|| (dynamic_cast<Top *>(newLhs) && dynamic_cast<Top *>(newRhs)) )
	 return new Top();
      else if (dynamic_cast<Top *>(newLhs) && dynamic_cast<Falsom*> (newRhs) )
	 return new Falsom();
      else
	 return new Implication (newLhs, newRhs);
   }

   virtual Formula* copy() const override
   {
      return new Implication (lhs->copy(), rhs->copy());
   }

   Formula* lhs;
   Formula* rhs;
};


struct Neg : public Formula
{
   Neg (Formula* _sub): sub(_sub) {}

   virtual bool eval (Variables& vars) const override
   {
      return false == sub->eval (vars);
   }

   virtual Formula* simplify () const override
   {
      // poenostavi vse spodaj
      Formula* newSub = sub->simplify();

      // ce je sub not, potem vrni sub.sub
      if (Neg* n = dynamic_cast<Neg*> (newSub)) {
	 return n->sub->copy();
	 delete newSub;
//      if (Neg* f = dynamic_cast<Neg *>(&newSub)) {
//	 return static_cast<Neg> (newSub).sub;
  //    }
      // ce je sub and, potem vrni or(not ...)
      } else if (And* f = dynamic_cast<And *>(newSub)) {
	 Or* o = new Or();

	 for (Formula* formula : f->sub) {
	    Neg n (formula);
	    o->sub.push_back (n.simplify());
	 }
	 delete newSub;
	 return o;
      // ce je sub or, potem vrni and(not ...)
      } else if (Or* f = dynamic_cast<Or *>(newSub)) {
	 And* a = new And();

	 for (Formula* formula : f->sub) {
	    Neg n (formula);
	    a->sub.push_back (n.simplify());
	 }
	 return a;
      } else
	 return new Neg (sub);
   }

   virtual Formula* copy() const override
   {
      return new Neg (sub->copy());
   }

   Formula* sub;
};





/*
// Consider this: is it better that disjunction and conjunction take two parameters, or should they accept an arbitrary list of parameters? If the accept a list, what do en empty conjunction and disjunction mean?

// - It would be better for them to take many as we don't get a recursion too deep that way

// TASK 2
struct Variable : public Formula
{
   Variable (string _name)
   { name = _name; }

   bool eval (Variables& vars)
   {
      using std::map;
      Variables::iterator it = vars.find (name);
      if (vars.end() == it)
	 throw runtime_error ("Variable not found");
      else
	 return it->second;
   }

   Formula simplify ()
   { return *this; }

   Formula tseytin()
   { return *this; }
private:
   string name;
};

// For simplifying:
// negation should be pushed to the bottom
//
*/
/* TASK 4:

   when working with SAT solvers, you'll want to have your formula
   in some normal form. If you do it naively with some distributivity,
   what will happen is that the formula will explode.


   What we will do is introduce additional variables
   For python dict: __hash__ (self) return hash(self.x)
*/


int main ()
{
   Falsom f;
//   Top t;

//   And f1 ({f, t});
}
