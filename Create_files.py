from random import * 
import os

def create_dimacs(num_variables, num_clauses, name):

    f = open(name, "w+")
    print("c " + name, file = f)
    print("p cnf " + str(num_variables) + " " + str(num_clauses), file = f)

    for i in range(num_clauses):
        #num = randint(1, num_variables)
        num = randint(1, 2) # number of atoms in one clause
        sam = sample(range(1, num_variables+1), num) 
        for i,s in enumerate(sam):
            sam[i] = choice([-1, 1])*s
        sam = map(str, sam)
        print(" ".join(sam) + " 0", file = f)

def create_files():
    for i in range(10):
        f = "Example" + str(i+1) + ".txt"
        create_dimacs(60, 100, f)
